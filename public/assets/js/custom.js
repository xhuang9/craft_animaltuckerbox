var updateModalAddToCartProduct;
$(document).ready(function ($) {
    "use strict";

    var ProductAjax = $('.product-ajax'),
        inputPriceMax = $('#priceMax'),
        inputPriceMin = $('#priceMin');

    var Number_of_product = ProductAjax.find('#NumberOfProduct').val();
    var sortByOption = ProductAjax.find('#sortByOption').val();

    var ValueToPass = '{{ set Number_of_product NUMBER }}{% set column = "title" %}';
    ValueToPass = ValueToPass.replace("NUMBER", Number_of_product);
    ValueToPass = ValueToPass.replace("title", sortByOption);
    $('.productSortingFilters').html(ValueToPass);


    /**
     * Using Ajax post Shopping cart update to server, so page don't refresh
     * Keep this same with the function trigger by click class "btn-product_addtocart"
     */
    $('.btn-quickview-addtocart').click(function(){
        console.log("form is clicked");
        updateModalAddToCartProduct();
        var form = $('.ProductAddToCart_quickview');
        var form_data = form.serializeArray();
        form_data.push({
            name: window.csrfTokenName,
            value: window.csrfTokenValue
        });
        var currentPageAddress = siteUrl + window.location.pathname.substr(1);
        console.log(form_data);
        $.ajax({
            type: 'post',
            url: currentPageAddress,
            data: form_data,
            success: function (response) {
                if (response.success) {
                    console.log(response.cart);
                    //update total cart price here
                }
            }
        });
    });


    /**
     * Using Ajax post Shopping cart update to server, so page don't refresh
     * Keep this same with the function trigger by click class "btn-quickview-addtocart"
     */
    $('.btn-product_addtocart').click(function(){
        console.log("form is clicked");
        updateModalAddToCartProduct_ProductPage();
        var form = $('.ProductAddToCart_product');
        var form_data = form.serializeArray();
        form_data.push({
            name: window.csrfTokenName,
            value: window.csrfTokenValue
        });
        var currentPageAddress = siteUrl + window.location.pathname.substr(1);
        console.log(form_data);
        $.ajax({
            type: 'post',
            url: currentPageAddress,
            data: form_data,
            success: function (response) {
                if (response.success) {
                    console.log(response.cart);
                    //update total cart price here
                }
            }
        });
    });

    ProductAjax.on("change",'select.sendProductFilter', function(){
        $.ajax({
            type: "POST",
            url: "http://craft.test/products/product.ajax.html",
            data: {
                sortByOption: ProductAjax.find('#sortByOption').val(),
                NumberOfProduct: ProductAjax.find('#NumberOfProduct').val(),
                MaxPrice: inputPriceMax.val(),
                MinPrice: inputPriceMin.val()
            },
            success: function(data) {
                console.log(data);
                $(".product-listing").html(data);
                var Number_of_product = ProductAjax.find('#NumberOfProduct').val();
                var sortByOption = ProductAjax.find('#sortByOption').val();

                var ValueToPass = '{% set Number_of_product "Number" %}{% set column = "title" %}';
                ValueToPass = ValueToPass.replace("Number", Number_of_product);
                ValueToPass = ValueToPass.replace("title", sortByOption);
                $('.productSortingFilters').html(ValueToPass);
                ProductAjax.find('#NumberOfProduct').val( Number_of_product );
                ProductAjax.find('#sortByOption').val( sortByOption );
            },
            error: function(xhr, status, error) {
                console.log(xhr);
                console.log(error);
                console.log(status);
            }
        });
    });

    /**
     * Click Class to Submit form
     */
    $('.cart-submit-delete').click(function(){
        $(".form-cart-delete").submit();
    });

    $('.cart-submit-update').click(function(){
        //Round is a count interval in the loop of cart.lineItems, it is helping locate the specific form in the loop to update
        console.log("button is clicked");
        if (typeof($(this).attr("data-value")) !== 'undefined'){
            var Round = $(this).attr("data-value");
            $(".form-cart-update-"+ Round).submit();
        }
    });

    $('.empty-the-cart').click(function(){
        $(".empty-the-cart-form").submit();
    });


    /**
     * Update Modal AddToCart with the information in Modal Quick View
     */

    var updateModalAddToCartProduct = function(){
        var modalAddToCartProduct = $('#modalAddToCartProduct');
        var ModalquickView = $('#ModalquickView');

        //update image
        var imgSrc = ModalquickView.find('.product-main-image img').attr('src');
        modalAddToCartProduct.find('.image-box img').attr('src', imgSrc);

        //update title
        var titleName = ModalquickView.find('.title').html();
        modalAddToCartProduct.find('.title').html(titleName);

        //update description
        var descriptionProduct = ModalquickView.find('.description .text').html();
        modalAddToCartProduct.find('.description').html(descriptionProduct);

        //update quantity
        var quantity = ModalquickView.find('.qty').val();
        modalAddToCartProduct.find('.qty span').html(quantity);

        var price = ModalquickView.find('.price').html().substr(1);
        var currency =  ModalquickView.find('.price').html().charAt(0);
        var totalPrice = parseFloat(price) * parseFloat(quantity);

        modalAddToCartProduct.find('.added-total span').html(currency + totalPrice);
    };


    /**
     * Update Modal AddToCart with the information in Product Page
     */
    var updateModalAddToCartProduct_ProductPage = function(){
        var modalAddToCartProduct = $('#modalAddToCartProduct');
        var productPageAddToCartContent = $('.productPageAddToCartContent');

        var imgSrc = productPageAddToCartContent.find('.ProductImageValue').val();
        modalAddToCartProduct.find('.image-box img').attr('src', imgSrc);

        var titleName = productPageAddToCartContent.find('.title').html();
        modalAddToCartProduct.find('.title').html(titleName);

        //update description
        var descriptionProduct = productPageAddToCartContent.find('.description p.text').html();
        modalAddToCartProduct.find('.description').html(descriptionProduct);

        //update quantity
        var quantity = productPageAddToCartContent.find('.qty').val();
        modalAddToCartProduct.find('.qty span').html(quantity);
    }

});

