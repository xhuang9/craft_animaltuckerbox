function quickViewCallback(property){
    if($('#ModalquickView').length) {
        $("#pageContent [data-target='#ModalquickView']").click(function(){
            var objProduct = $(this).parents('.product');
            var objPopupProduct = $('#ModalquickView');
            //show image on quick view section
            if(objProduct.find('.image-box img').length){
                var imgSrc = objProduct.find('.image-box img').attr('src');
                objPopupProduct.find('.product-main-image img').attr('src', imgSrc);
                //pass image url to hidden input to post to server
                objPopupProduct.find('.ProductImageUrl option').attr('value', imgSrc);
            };


            if(objProduct.find('.title a').length){
                var titleName = objProduct.find('.title a').html();
                objPopupProduct.find('.title').html(titleName);
            } else{
                var titleName = objProduct.find('.title').html();
                objPopupProduct.find('.title').html(titleName);
            };
            if(objProduct.find('.price .new-price').length){
                var priceProduct = objProduct.find('.price').html();
                objPopupProduct.find('.price').html(priceProduct);
            } else{
                var priceProduct = objProduct.find('.price').html();
                objPopupProduct.find('.price').html(priceProduct);
            };

            if(objProduct.find('.product-sku').length){
                var ProductSku = objProduct.find('.product-sku').html();
                objPopupProduct.find('.sku span.value').html(ProductSku);
            } else{
                var ProductSku = objProduct.find('.sku span.value').html();
                objPopupProduct.find('.sku span.value').html(ProductSku);
            };

            if(objProduct.find('.product-id').length){
                var ProductId = objProduct.find('.product-id').html();
                objPopupProduct.find('.product-id').val(ProductId);
            } else{
                var ProductId = objProduct.find('.product-id').val();
                objPopupProduct.find('.product-id').val(ProductId);
            };

            if(objProduct.find('.availability').length){
                var ProductSku = objProduct.find('.availability').html();
                objPopupProduct.find('.availability span.value').html(ProductSku);
            } else{
                var ProductSku = objProduct.find('.availability span.value').html();
                objPopupProduct.find('.availability span.value').html(ProductSku);
            };


            if(objProduct.find('.description').length){
                var descriptionProduct = objProduct.find('.description').html();
                objPopupProduct.find('.description .text').html(descriptionProduct);
            };
        });
    }
}
quickViewCallback();
