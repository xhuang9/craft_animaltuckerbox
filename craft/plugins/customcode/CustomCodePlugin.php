<?php
/**
 * CustomCode plugin for Craft CMS
 *
 * Custom PHP Code for Craft CMS
 *
 * @author    Parry
 * @copyright Copyright (c) 2018 Parry
 * @link      http://www.mthreemedia.com.au
 * @package   CustomCode
 * @since     1.0.0
 */

namespace Craft;

class CustomCodePlugin extends BasePlugin
{
    /**
     * @return mixed
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
         return Craft::t('CustomCode');
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return Craft::t('Custom PHP Code for Craft CMS');
    }

    /**
     * @return string
     */
    public function getDocumentationUrl()
    {
        return '???';
    }

    /**
     * @return string
     */
    public function getReleaseFeedUrl()
    {
        return '???';
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return '1.0.0';
    }

    /**
     * @return string
     */
    public function getSchemaVersion()
    {
        return '1.0.0';
    }

    /**
     * @return string
     */
    public function getDeveloper()
    {
        return 'Parry';
    }

    /**
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'http://www.mthreemedia.com.au';
    }

    /**
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }

    /**
     */
    public function onBeforeInstall()
    {
    }

    /**
     */
    public function onAfterInstall()
    {
    }

    /**
     */
    public function onBeforeUninstall()
    {
    }

    /**
     */
    public function onAfterUninstall()
    {
    }
}