<?php
/**
 * CustomCode plugin for Craft CMS
 *
 * CustomCode Translation
 *
 * @author    Parry
 * @copyright Copyright (c) 2018 Parry
 * @link      http://www.mthreemedia.com.au
 * @package   CustomCode
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
