# CustomCode plugin for Craft CMS

Custom PHP Code for Craft CMS

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install CustomCode, follow these steps:

1. Download & unzip the file and place the `customcode` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /customcode`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `customcode` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

CustomCode works on Craft 2.4.x and Craft 2.5.x.

## CustomCode Overview

-Insert text here-

## Configuring CustomCode

-Insert text here-

## Using CustomCode

-Insert text here-

## CustomCode Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Parry](http://www.mthreemedia.com.au)
