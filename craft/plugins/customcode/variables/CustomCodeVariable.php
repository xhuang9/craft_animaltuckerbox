<?php
/**
 * CustomCode plugin for Craft CMS
 *
 * CustomCode Variable
 *
 * @author    Parry
 * @copyright Copyright (c) 2018 Parry
 * @link      http://www.mthreemedia.com.au
 * @package   CustomCode
 * @since     1.0.0
 */

namespace Craft;

use FontLib\Table\Type\post;

class CustomCodeVariable
{
    /**
     * determine availability of a product, using variants
     * @param $fieldName
     * @param $variants
     * @param bool $onlyInStock
     * @return mixed|string
     */
    public function getVariantCheckboxValue($fieldName, $variants, $onlyInStock=false) {

        $checked = "";
        foreach($variants as $variant) {
            $outOfStock = (($variant->stock <= 0) && ($variant->unlimitedStock == false));

            if (!$onlyInStock || ($onlyInStock && !$outOfStock)) {
                if (isset($variant->{$fieldName}[0])) {
                   $checked =  print_r($variant->{$fieldName}[0]->selected, true);
                }
            }
        }

        return $checked;
    }

    /**
     * check whether a product is in stock
     * @param $unlimited
     * @param $stockquantity
     * @return bool
     */
    public function checkInStock($unlimited, $stockquantity){
        if($unlimited != ""){
            return true;
        }else if($unlimited == ""){
            if($stockquantity > 0){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * sort a multidimentional array by one of its column
     * @param array $arr
     * @param string $col
     * @param int $dir  sort order
     * @return mixed
     */
    // $dir could be SORT_ASC or SORT_DESC
    public function arraySortByColumn($arr, $col, $dir = SORT_ASC) {
        if($dir=="SORT_ASC"){
            $order = SORT_ASC;
        }
        if($dir=="SORT_DESC"){
            $order = SORT_ASC;
        }
        array($arr);
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $order, $arr);
        return $arr;
    }

    /**
     * retrieve post value
     * @type string
     * @return mixed
     */
    public function receivePostValue($key){
        if(isset($_POST[$key])) {
            return $_POST[$key];
        }else{
            return false;
        }
    }

    public function setSessionVariable($key, $value){
//        session_start();
        $_SESSION[$key] = $value;
//        craft()->userSession->setFlash($key, $value);
    }

    public function getSessionVariable($key){
//        session_start();
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }else{
            return null;
        }
    }


    /**
     * unset a variable from array
     * @param $value      if value is found in the array remove that element
     * @param $array      given $array
     * @return mixed      processed array
     */
    public function unsetVariableFromArray($value, $array)
    {
        $count = 0;
        foreach ($array as $val) {
            if ($val == $value) {
                array_splice($array, $count, 1);
            }
            $count++;
        }
        return $array;
    }

    /**
     * convert array to a url output able string
     * @param $array
     * @return string
     */
    public function convertArrayToUrlString($array){
        $str = "";
        foreach($array as $key => $value){
            $str = $str."_".$key."=".$value;
        }
        return $str;
    }

    /**
     * convert array to string by separator
     * @param $string
     * @param $separator
     * @return array
     */
    public function convertStringToArray($string, $separator){
        return explode($separator, $string);
    }

    public function convertArrayToString($array, $separator){
        return implode($separator, $array);
    }

}
