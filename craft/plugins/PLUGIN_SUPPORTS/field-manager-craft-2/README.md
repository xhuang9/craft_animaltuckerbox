> 
> ### Field Manager has a new home with the fine folks at Verbb. Read about it in our [blog post](https://verbb.io/blog/welcome-to-verbb).
>

# Field Manager Plugin for Craft CMS

<img width="500" src="https://verbb.io/uploads/plugins/field-manager/_800x455_crop_center-center/field-manager-social-card.png">

Field Manager is a Craft CMS plugin to help make it easy to manage your fields and field groups. 

<img src="https://raw.githubusercontent.com/verbb/field-manager/craft-2/screenshots/main.png" />

## Documentation

Visit the [Field Manager Plugin page](https://verbb.io/craft-plugins/field-manager) for all documentation, guides, pricing and developer resources.

## Support

Get in touch with us via the [Field Manager Support page](https://verbb.io/craft-plugins/field-manager/support) or by [creating a Github issue](/verbb/field-manager/issues)

<h2></h2>

<a href="https://verbb.io" target="_blank">
  <img width="100" src="https://verbb.io/assets/img/verbb-pill.svg">
</a>




