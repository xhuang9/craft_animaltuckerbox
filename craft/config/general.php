<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'omitScriptNameInUrls'     => true, // clean urls
        'backupDbOnUpdate'         => true, // safety mesure
        'maxUploadFileSize'        => 32000000, // 32mb
//        'cpTrigger' => 'mmg-creative',          // Control Panel trigger word
//        'defaultWeekStartDay' => 0,             // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'useCompressedJs'        => false,
        // 'errorTemplatePrefix' => "pages/",
//        'mcsubApikey' => 'df347a07a17e77aa868ec848d19cb4bf-us15',
//        'mcsubListId' => 'cd533f5022',
        'mcsubDoubleOptIn' => false,
        'environmentVariables' => array(
            'assetsBaseUrl' => '/assets',
            'assetsBasePath' => './assets',
            'baseUrl'  => 'http://craft.test/',
        )
    ),

    // local
    'craft.test' => array(
        'devMode' => true,
        'userSessionDuration' => false,
        'useSslOnTokenizedUrls' => false,
        'omitScriptNameInUrls' => true,
        'autoLoginAfterAccountActivation' => true,
//        'enableCsrfProtection' => true,
        'logDumpMethod' => 'print_r',
        'timezone' => 'Australia/Hobart',
        'siteUrl' => 'http://craft.test',
        'siteName' => 'craft',
        'environmentVariables' => array(
            'assetsBaseUrl' => 'http://craft.test/assets',
            'assetsBasePath' => './assets',
            'baseUrl'  => 'http://craft.test/',
        )
    ),

    // Staging
    '.mthreemedia.com.au' => array(
        'devMode' => false,
        'userSessionDuration' => false,
        'siteUrl' => 'http://.mthreemedia.com.au',
        'siteName' => '',
        'environmentVariables' => array(
            'assetsBaseUrl' => '/assets',
            'assetsBasePath' => './assets',
            'baseUrl'  => ''
        )
    ),
    //
    // // live
    '.com.au' => array(
        'devMode' => false,
        'userSessionDuration' => false,
        'siteUrl' => 'http://.com.au',
        'siteName' => '',
    ),

);